var gulp = require('gulp');
var del = require('del');
var config = require('../config');

var isLocal = config.env.isLocal;
var isProduction = config.env.isProduction;
var paths = isLocal ? config.localPaths : config.paths;

gulp.task('clean:jade', function() {
	return del([
		paths.pages + 'layout/blocks.jade',
		paths.pages + 'layout/vendor-js.jade'
	], {force: true});
});

gulp.task('clean:scss', function() {
	return del([
		paths.css + 'adaptive.css',
		paths.css + 'fontawesome.css',
		paths.css + 'fonts.css',
		paths.css + 'global.css',
		paths.css + 'optimize.css',
		paths.css + 'tech.css',
		paths.css + 'print.css',
		paths.sass + '_blocks.scss'
	], {force: true});
});

gulp.task('clean:html', function() {
	if(isProduction) { return; }
	return del([
		paths.base + '*.html'
	]);
});

gulp.task('clean:js', function() {
	return del([
		paths.js + 'blocks.js',
		paths.js + 'blocks.dummy.js'
	], {force: true});
});

gulp.task('clean:prodjs', function() {
	if(!isProduction) { return; }
	if(isLocal) { return; }
	return del([
		paths.js + 'global.js',
		paths.js + 'script.js',
		paths.js + '/vendor/*'
	], {force: true});
});

gulp.task('clean', ['clean:jade', 'clean:scss', 'clean:html', 'clean:js', 'clean:prodjs']);
